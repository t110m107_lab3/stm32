//
// Created by ilia.motornyi on 13-Dec-18.
//

#ifndef __SUPPORT_H
#define __SUPPORT_H

#include "main.h"


#define HEX_CHARS      "0123456789ABCDEF"
#define nRF24_WAIT_TIMEOUT         (uint32_t)0x000FFFFF


extern SPI_HandleTypeDef hspi1;

/*
#define NRF_CE_Pin GPIO_PIN_7
#define NRF_CE_GPIO_Port GPIOC   //401RE
#define NRF_CSN_Pin GPIO_PIN_6   //
#define NRF_CSN_GPIO_Port GPIOB
*/

//#define NRF_CE_Pin GPIO_PIN_4
//#define NRF_CE_GPIO_Port GPIOA   //Bluepill
//#define NRF_CSN_Pin GPIO_PIN_0   //
//#define NRF_CSN_GPIO_Port GPIOB

//#define NRF_CE_Pin GPIO_PIN_5
//#define NRF_CE_GPIO_Port GPIOA   //F072
//#define NRF_CSN_Pin GPIO_PIN_4   //
//#define NRF_CSN_GPIO_Port GPIOA

//#define NRF_CE_Pin GPIO_PIN_5
//#define NRF_CE_GPIO_Port GPIOA
//#define NRF_CSN_Pin NRF_CSN_Pin
//#define NRF_CSN_GPIO_Port GPIOA


#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0         ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                 1 bit  for subpriority */
#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                 0 bit  for subpriority */
#endif



extern SPI_HandleTypeDef hspi2;


void runRadio(uint8_t ROLE);

void TX_single(void);
void RX_single(void);
void UART_SendStr(char *string);
void UART_SendChar(char b);
void read_ADC(void);
void adc_start(void);
void Toggle_LED() ;





typedef enum {
	nRF24_TX_ERROR  = (uint8_t)0x00, // Unknown error
	nRF24_TX_SUCCESS,                // Packet has been transmitted successfully
	nRF24_TX_TIMEOUT,                // It was timeout during packet transmit
	nRF24_TX_MAXRT                   // Transmit failed with maximum auto retransmit count
} nRF24_TXResult;

extern nRF24_TXResult tx_res;

nRF24_TXResult nRF24_TransmitPacket(uint8_t *pBuf, uint8_t length);


static inline void nRF24_CE_L() {
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_RESET);
}

static inline void nRF24_CE_H() {
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_SET);
}

static inline void nRF24_CSN_L() {
    HAL_GPIO_WritePin(NRF_CSN_GPIO_Port, NRF_CSN_Pin, GPIO_PIN_RESET);
}

static inline void nRF24_CSN_H() {
    HAL_GPIO_WritePin(NRF_CSN_GPIO_Port, NRF_CSN_Pin, GPIO_PIN_SET);
}


static inline uint8_t nRF24_LL_RW(uint8_t data) {
    // Wait until TX buffer is empty
    uint8_t result;
    if(HAL_SPI_TransmitReceive(&hspi1,&data,&result,1,2000)!=HAL_OK) {
        Error_Handler();
    };
    return result;
}


static inline void Delay_ms(uint32_t ms) { HAL_Delay(ms); }

#endif //__SUPPORT_H
